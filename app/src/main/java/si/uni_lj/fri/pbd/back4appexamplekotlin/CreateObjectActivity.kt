package si.uni_lj.fri.pbd.back4appexamplekotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseObject
import si.uni_lj.fri.pbd.back4appexamplekotlin.databinding.ActivityCreateObjectBinding

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CreateObjectActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreateObjectBinding

    companion object {
        const val TAG = "CreateObjectActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateObjectBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        var formattedDate: Date? = null

        binding.calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val getDate = dayOfMonth.toString() + "/" + (month + 1) + "/" + year
            formattedDate = convertStringToDate(getDate)
        }

        binding.btnCreate.setOnClickListener {

            var validationError = false

            val validationErrorMessage = StringBuilder("Please, ")

            if (isEmptyText(binding.edtItem)) {
                validationError = true
                validationErrorMessage.append("insert name")
            }

            if (isEmptyDate(formattedDate)) {
                if (validationError) {
                    validationErrorMessage.append(" and ")
                }
                validationError = true
                validationErrorMessage.append("select a date")
            }

            validationErrorMessage.append(".")

            if (validationError) {
                Toast.makeText(this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show()
            } else {
                saveObject(formattedDate)
            }
        }

    }

    private fun convertStringToDate(getDate: String): Date? {
        var today: Date? = null
        val simpleDate = SimpleDateFormat("dd/MM/yyyy")

        try {
            today = simpleDate.parse(getDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return today
    }

    private fun isEmptyText(text: EditText): Boolean {
        return text.text.toString().trim().isEmpty()
    }

    private fun isEmptyDate(date: Date?): Boolean {
        return date?.toString() == "null"
    }

    private fun saveObject(formattedDate : Date?) {
        Log.d(TAG, "Save object")

        // TODO: Save item



    }
}