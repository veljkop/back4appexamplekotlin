package si.uni_lj.fri.pbd.back4appexamplekotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.GetCallback
import com.parse.ParseObject
import com.parse.ParseQuery
import si.uni_lj.fri.pbd.back4appexamplekotlin.databinding.ActivityUpdateObjectBinding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class UpdateObjectActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateObjectBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateObjectBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val objectName = intent.getStringExtra("objectName")

        var objectId: String? = null

        var formattedDate: Date? = null

        binding.calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val getDate = dayOfMonth.toString() + "/" + (month + 1) + "/" + year
            formattedDate = convertStringToDate(getDate)
        }

        binding.btnCreate.setOnClickListener { saveObject(objectId, formattedDate) }

        val query: ParseQuery<ParseObject> = ParseQuery.getQuery("ReminderList")
        query.whereEqualTo("itemName", objectName)
        query.getFirstInBackground { parseObject, e ->
            if (e == null) {
                val dateCommitment = parseObject.getDate("dateCommitment")
                if (dateCommitment != null) {
                    binding.calendarView.setDate(dateCommitment.time, true, true)
                }
                binding.edtItem.setText(objectName)
                binding.edtAdditionalInformation.setText(parseObject.getString("additionalInformation"))
                binding.swiAvailable.isChecked = parseObject.getBoolean("isAvailable")
                objectId = parseObject.objectId
            } else {
                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
            }

        }
    }


    private fun saveObject(objectId: String?, formattedDate: Date?) {

        val query: ParseQuery<ParseObject> = ParseQuery.getQuery("ReminderList")

        query.getInBackground(objectId) { reminderList, e ->
            if (e == null) {
                reminderList.put("itemName", binding.edtItem.text.toString())
                reminderList.put("additionalInformation", binding.edtAdditionalInformation.text.toString())
                if (formattedDate != null) {
                    reminderList.put("dateCommitment", formattedDate)
                }
                reminderList.put("isAvailable", binding.swiAvailable.isChecked)
                reminderList.saveInBackground {
                    if(it == null) {
                        startActivity(Intent(this@UpdateObjectActivity, ReadObjectsActivity::class.java))
                    } else {
                        Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }


    private fun convertStringToDate(getDate: String): Date? {
        var today: Date? = null
        val simpleDate = SimpleDateFormat("dd/MM/yyyy")

        try {
            today = simpleDate.parse(getDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return today
    }

}