package si.uni_lj.fri.pbd.back4appexamplekotlin

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseObject
import com.parse.ParseQuery
import si.uni_lj.fri.pbd.back4appexamplekotlin.databinding.ActivityReadObjectsBinding

class ReadObjectsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReadObjectsBinding

    var dataList = ArrayList<String>()
    var myArray = arrayOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadObjectsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.fab.setOnClickListener {
            val intent = Intent(this@ReadObjectsActivity, CreateObjectActivity::class.java)
            startActivity(intent)
        }

        findObjects()
    }

    private fun findObjects() {

        myArray = arrayOf()

        // Configure Query
        val query: ParseQuery<ParseObject> = ParseQuery.getQuery("ReminderList")
        // Sorts the results in ascending order by the itemName field
        query.orderByAscending("itemName")

        query.findInBackground { objects, e ->
            if (e == null) {
                for (obj in objects) {
                    val element: String? = obj?.getString("itemName")
                    if (element!=null) dataList.add(element)
                }
            }

            myArray = dataList.toTypedArray()

            binding.lvItems.adapter = ArrayAdapter(this@ReadObjectsActivity,
                    android.R.layout.simple_list_item_single_choice, myArray)

            binding.lvItems.setOnItemClickListener { adapter, view, position, id ->
                val value = adapter.getItemAtPosition(position) as String

                //Alert showing the options related with the object (Update or Delete)
                val builder = AlertDialog.Builder(this@ReadObjectsActivity)
                    .setTitle("$value movie")
                    .setMessage("What do you want to do?")
                    .setPositiveButton("Delete") { dialog, which ->
                        dataList.removeAt(position)
                        deleteObject(value)
                        myArray = dataList.toTypedArray()
                        binding.lvItems.adapter = ArrayAdapter(
                                this@ReadObjectsActivity,
                                android.R.layout.simple_list_item_single_choice,
                                myArray
                        )
                    }
                    .setNeutralButton("Update") { dialog, which ->
                        val intent = Intent(
                            this@ReadObjectsActivity,
                            UpdateObjectActivity::class.java
                        )
                        //Send string value to UpdateObjectActivity Activity with putExtra Method
                        intent.putExtra("objectName", value)
                        startActivity(intent)
                    }.setNegativeButton("Cancel"){ dialog, which ->
                        dialog.cancel()
                    }
                val ok: AlertDialog = builder.create()
                ok.show()
            }
        }
    }

    private fun deleteObject(value: String) {

        val query: ParseQuery<ParseObject> = ParseQuery.getQuery("ReminderList")
        query.whereEqualTo("itemName", value)
        
        query.findInBackground { objects, e ->
            if (e == null) {
                objects[0].deleteInBackground(){
                    if (it != null) {
                        Toast.makeText(applicationContext,it.message,Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                Toast.makeText(applicationContext,e.message,Toast.LENGTH_LONG).show()
            }
        }

    }

}